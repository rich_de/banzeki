# BAnzeKi

Bestückungs-Anzeige für KiCAD
Placement indicator for KiCAD

Written and (C) 2019 by Richard Theil

### Introduction
Thank you for choosing BAnzeKi as the comprehensive solution for all your
KiCAD placement needs.

Not.

I hacked out all of this in about two days after getting too frustrated
with getting VisualPlace to work under Wine. It is just barely usable
enough that it works for me. That said, it works for me rather nicely,
so I thought I'd share what I have to possibly spare fellow KiCAD users
similar frustration. Still, if it messes up and you end up with a container
full of brand new PCB trash, or the Elbonians get to steal your corporate
secrets, it is not my fault.

BAnzeKi scratches two itches of mine:

- Guide a worker through placement of components (mostly for THT)
- Export a CSV file that includes THT parts (KiCAD has issues here)

### How to use
No need to mess around with Gerbers, or any intermediate data, just
open a KiCAD PCB file.

There is a list with the parts that can be sorted by reference (for looking
up a specific part, or preferrably for exporting the CSV), location (top-down,
might help with auto p+p), or by my preferred placement order.

The preferred placement order tries hard at being smart, but some information
will always be missing. As it can't be smart enough, you'll still have to
navigate to suitable positions in the list to work from a few times.

Components without a value, or with the value starting with '(' will be moved
to the end, so they don't show up when going through the list. SMD (for
reflow) will come before THT (for hand soldering). Component classes (i.e.
the starting letters of the references) are grouped, as are values of
components in ascending order (but not mixing different packages).

Next to the list is a diagram of the board. This diagram is made from the
lines in the Edge.Cuts and F.Fab layers (the lines worked well enough for me
and I didn't feel like spending time on other graphical elements).
References will show at a part's origin, and with the part's rotation, which
is not like KiCAD does it at an offset, but can be considered a feature,
because it indicates the true origin.

When an item in the list is selected, it is shown in bold red, with big
crosshairs. The top crosshair line is longer than the others (and the bottom
one is slightly shorter), so there is immediate visual feedback on the
rotation.

### Footswitch
To keep worker hands free during assembly, a foot switch can be used
to step through the list. It needs to be configured to send "cursor down".
I tested a "Microdia" footswitch, USB ID ID 0c45:7403, which could be
configured with the [footswitch](https://github.com/rgerganov/footswitch) utility by Radoslav Gerganov. The command line for the utility is:
```sh
$ sudo footswitch -k down
```

### Building
I built with Qt Creator 4.5.2 against Qt 5.9.5 on Mint 19, but I don't use
any exotic or platform specific features, so similar versions will likely
work well, and on other platforms, too.

### Maintaining
The premise when writing this software was that it would take less time
and frustration to write than it would take to get VisualPlace to work
properly, so it really has been a quick & dirty hack. Software architecture
is absent, there are roughly zero error checks and fuzzing input data will
probably get you remote root.

So please spare me any bug reports. Instead of sending patches and
contributions to me, please fork this project and maintain it yourself.
Do tell me when you did this, though. :)

Also feel free to cross compile and package it as you like. I would much
appreciate, if, in two years, when I need it again, I could install it
with a click in Synaptic.

I hope I made clear that I don't want to spend any more time on this. If you
still think you that absolutely need ME to implement a custom feature, or
provide whatever services, drop me a line, but be prepared to be paying
rather uncompetetive rates.

### License
BAnzeKi is licensed under GPL version 2.


