/*
baview.h
Copyright (C) 2019 Richard Theil

This program is free software; you can redistribute it and/or
modify it under the terms of version 2 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BAVIEW_H
#define BAVIEW_H

#include <QWidget>
#include <QAbstractTableModel>

typedef struct {
    double x, y;
}
PointRec;

typedef struct {
    PointRec from, to;
    double width;
}
LineRec;


class BAModule
{
public:

    BAModule();

    QString ref, val, package;
    QList<LineRec> fablines;
    double x, y, rot;
    int mountType;
    bool selected;

    void setRef( QString iref );
    void setValue( QString ival );
    void setPackage( QString ipack );

    QString refType;
    bool valueGood;
    double valueNumeric;
};

class BAModuleListModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    QList<BAModule> *list;

    BAModuleListModel(QObject *parent = 0);
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index = QModelIndex(), int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};

class BAView : public QWidget
{
    Q_OBJECT
public:
    double scale;

    QList<BAModule> *list;
    QList<LineRec> *edges;

    explicit BAView(QWidget *parent = nullptr);

    void adaptToData(void);

    void paintLine(QPainter &ptr, LineRec &lrec);
    void paintLineList( QPainter &ptr, QList<LineRec> &llist );

    virtual void paintEvent(QPaintEvent *event);

signals:

public slots:
};

#endif // BAVIEW_H
