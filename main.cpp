#include "banzwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    BanzWindow w;
    w.show();

    return a.exec();
}
