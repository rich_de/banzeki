/*
sexpr.cpp
Copyright (C) 2019 Richard Theil

This program is free software; you can redistribute it and/or
modify it under the terms of version 2 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.
*/

#include "sexpr.h"
#include <assert.h>
#include <stdio.h>

#include <QDebug>

SExpr::SExpr()
{
    refCount = 0;
    typ = typList;
    list = new QList<SExpr*>;
    str = "";
}

SExpr::SExpr(QString iText, bool isString)
{
    refCount = 0;
    typ = isString ? typString : typSymbol;
    list = NULL;
    str = iText;
}

SExpr::~SExpr()
{
    if (list) {
        foreach( SExpr* s, *list ) s->free();
        delete list;
    }
}

SExpr* SExpr::copy(void)
{
    refCount++;
    return this;
}

void SExpr::free(void)
{
    if (!refCount)
        delete this;
    else
        refCount--;
}

QString SExpr::text(void)
{
    assert(!list);
    return str;
}

int SExpr::count(void)
{
    assert(list);
    return list->count();
}

SExpr* SExpr::at(int i)
{
    assert(list);
    return list->at(i);
}

void SExpr::append(SExpr* item)
{
    assert(list);
    list->append(item);
}

SExpr* SExpr::filterByFirst(QString sym)
{
    SExpr *resultList = new SExpr();

    assert(list);
    foreach( SExpr* item, *list ) {
        if (item->list && item->count() >= 1) {
            SExpr* ss = item->at(0);
            if (!ss->list && (ss->text() == sym))
                resultList->append(item->copy());
        }
    }

    return resultList;
}

SExpr* SExpr::filterByFirst2(QString sym0, QString sym1)
{
    SExpr *resultList = new SExpr();

    assert(list);
    foreach( SExpr* item, *list ) {
        if (item->list && item->count() >= 2) {
            SExpr* s0 = item->at(0);
            SExpr* s1 = item->at(1);
            if (!s0->list && (s0->text() == sym0) && !s1->list && (s1->text() == sym1))
                resultList->append(item->copy());
        }
    }

    return resultList;
}

SExpr* SExpr::findByFirst(QString sym)
{
    assert(list);
    foreach( SExpr* item, *list ) {
        if (item->list && item->count() >= 1) {
            SExpr* ss = item->at(0);
            if (!ss->list && (ss->text() == sym))
                return item;
        }
    }

    return NULL;
}

SExpr* SExpr::findByFirst2(QString sym0, QString sym1)
{
    assert(list);
    foreach( SExpr* item, *list ) {
        if (item->list && item->count() >= 2) {
            SExpr* s0 = item->at(0);
            SExpr* s1 = item->at(1);
            if (!s0->list && (s0->text() == sym0) && !s1->list && (s1->text() == sym1))
               return item;
        }
    }

    return NULL;
}


static void indent( QTextStream *cout, int level )
{
    for (int i = 0; i < level; i++)
        *cout << "  ";
}

void SExpr::write( QTextStream *cout, int level )
{
    SExpr *elem;
    int i;

    if (list) {

        // shortcut check for flat output (could count recursive nodes?)
        if (list->count() < 5) {
            for (i = 0; i < list->count(); i++) {
                elem = list->at(i);
                if (elem->list)
                    break;
            }
            if (i == list->count()) {
                indent(cout,level);
                *cout << "(";
                for (i = 0; i < list->count(); i++) {
                    if (i != 0)
                        *cout << " ";
                    elem = list->at(i);
                    *cout << elem->text();
                }
                *cout << ")\n";
                cout->flush();
                return;
            }
        }

        indent( cout, level );
        *cout << "(\n";   // up to our level
        for (i = 0; i < list->count(); i++) {
            elem = list->at(i);
            elem->write(cout, level+1);
        }
        indent( cout, level );
        *cout << ")\n";
    }
    else {
        indent( cout, level );
        *cout << this->text();
        *cout << "\n";
    }
    cout->flush();
}

void SExpr::print(void)
{
    QTextStream cout(stdout, QIODevice::WriteOnly);
    write(&cout,0);
    cout << "\n";
    cout.flush();
}


SExprLoader::SExprLoader( QTextStream *iTextStream )
{
    textStream = iTextStream;
    c = -1;
    lineNumber = 1;
    nextChar();
}

void SExprLoader::nextChar(void)
{
    if (textStream->atEnd())
        c = -1;
    else {
        *textStream >> c;
        // char ch = c.toLatin1();
        // putc( ch , stderr );
        if (c == '\n' || c == '\r')
            lineNumber++;
    }
}

void SExprLoader::error(QString err)
{
    qCritical() << err;
    exit(1);
}

void SExprLoader::skipWhite(void)
{
    while (c.isSpace())
        nextChar();
}

SExpr* SExprLoader::loadList( void )
{
    SExpr* result = NULL, *item = NULL;
    nextChar(); // skip '('

    // fprintf( stderr, "BEGIN %d\n", ++level);
    result = new SExpr();
    for (;;)
    {
        skipWhite();
        if (c == -1)
            error("unexpected EOF in list");
        if (c == ')') {
            nextChar();
            break;
        }
        item = load();
        result->append(item);
    }
    // fprintf( stderr, "END(%d) %d\n",result->count(), level--);

    return result;
}

#define LSBUFSIZE 256
SExpr* SExprLoader::loadString( void )
{
    nextChar(); // skip quote

    QChar buf[LSBUFSIZE];
    int n = 0;
    QString str;
    for (;;)
    {
        if (c == -1)
            error("unexpected EOF in string");
        if (c == '"') {
            nextChar();
            break;
        }
        buf[n++] = c;
        nextChar();
        if (n == LSBUFSIZE) {
            str += QString( buf, n );
            n = 0;
        }
    }

    str += QString( buf, n );
    // fprintf( stderr, "STRING '%s'", str.toUtf8().constData());
    SExpr* result = new SExpr(str,true);
    return result;
}

SExpr* SExprLoader::loadSymbol( void )
{
    QString str;

    assert( c != ')');

    QChar buf[LSBUFSIZE];
    int n = 1;
    buf[0] = c;
    nextChar();

    for (;;)
    {
        if (c == -1)
            error("unexpected EOF in symbol");
        if (c.isSpace() || (c == ')')) {
            break;
        }

        buf[n++] = c;
        nextChar();
        if (n == LSBUFSIZE) {
            str += QString( buf, n );
            n = 0;
        }
    }

    str += QString( buf, n );
    // fprintf( stderr, "SYMBOL '%s'", str.toUtf8().constData());
    SExpr* result = new SExpr(str,true);
    return result;
}

SExpr* SExprLoader::load( void )
{
    skipWhite();
    // fprintf( stderr, "load '%c' %04X\n", c.toLatin1(), c.unicode());
    if (c == '(')
        return loadList();
    else if (c == '"')
        return loadString();
    else if (c != -1)
        return loadSymbol();
    else
        error("unexpected EOF");
    return NULL;
}
