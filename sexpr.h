/*
sexpr.h
Copyright (C) 2019 Richard Theil

This program is free software; you can redistribute it and/or
modify it under the terms of version 2 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SEXPR_H
#define SEXPR_H

#include <QString>
#include <QList>
#include <QTextStream>

typedef enum {
    typList,
    typSymbol,
    typString
} SExprType;

class SExpr
{
public:
    int refCount;
    SExprType typ;
    QList<SExpr*> *list;
    QString str;

    SExpr();
    SExpr(QString iText, bool isString);
    ~SExpr();

    SExpr* copy(void);
    void free(void);

    QString text(void);
    int count(void);
    SExpr* at(int i);
    void append(SExpr* item);

    SExpr* filterByFirst(QString sym);
    SExpr* filterByFirst2(QString sym0, QString sym1);  // must dispose

    SExpr* findByFirst(QString sym);
    SExpr* findByFirst2(QString sym0, QString sym1);    // must not dispose

    void write( QTextStream *cout, int level );
    void print(void);
};

class SExprLoader
{
public:
    QTextStream     *textStream;
    QChar           c;
    int             lineNumber;

    SExprLoader( QTextStream *iTextStream );

    void error(QString err);

    void nextChar(void);
    void skipWhite(void);

    SExpr* loadList( void );
    SExpr* loadString( void );
    SExpr* loadSymbol( void );

    SExpr* load( void );
};

#endif // SEXPR_H
