/*
banzwindow.cpp
Copyright (C) 2019 Richard Theil

This program is free software; you can redistribute it and/or
modify it under the terms of version 2 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.
*/

#include "banzwindow.h"
#include "ui_banzwindow.h"

#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QCollator>
#include <QMessageBox>

void bumpBounds( const PointRec &p, PointRec &tl, PointRec &br, bool &bumping )
{
    if (!bumping) {
        tl = p;
        br = p;
        bumping = true;
    } else {
        if (p.x < tl.x)
            tl.x = p.x;
        if (p.y < tl.y)
            tl.y = p.y;
        if (p.x > br.x)
            br.x = p.x;
        if (p.y > br.y)
            br.y = p.y;
    }
}

bool getImageBounds( QList<LineRec> &lines, PointRec &tl, PointRec &br )
{
    bool bumping = false;
    tl.x = 0;
    tl.y = 0;
    br.x = 0;
    br.y = 0;
    foreach( const LineRec &line, lines ) {
        bumpBounds( line.from, tl, br, bumping );
        bumpBounds( line.to, tl, br, bumping );
    }
    return bumping;
}

bool getLineParams( SExpr* expr, LineRec &outLine )
{
    SExpr *px = expr->findByFirst("start");
    if (!px)
        return false;
    outLine.from.x = px->at(1)->text().toDouble();
    outLine.from.y = px->at(2)->text().toDouble();

    px = expr->findByFirst("end");
    if (!px)
        return false;
    outLine.to.x = px->at(1)->text().toDouble();
    outLine.to.y = px->at(2)->text().toDouble();

    px = expr->findByFirst("width");
    if (!px)
        outLine.width = 0.0;
    else
        outLine.width =  px->at(1)->text().toDouble();

    return true;
}

// wants pre-filtered list of lines
void getLineList( SExpr* expr, QList<LineRec> &lines, QString layer )
{
    for (int i = 0; i < expr->count(); i++) {
        SExpr *ln = expr->at(i);
        SExpr *layx = ln->findByFirst("layer");
        if (layx) {
            QString lineLayer = layx->at(1)->text();
            if (lineLayer == layer) {
                LineRec lrec;
                if (getLineParams( ln, lrec )) {
                    lines.append(lrec);
                }
            }
        }
    }
}


static int mountOrder( int mountType )
{
    // SMD THT orderlines
    //          4 last    (like mounting holes)
    //  Y       1 first   (like p&p smd)
    //      Y   3 third   (like big electrolytics)
    //  Y   Y   2 second  (like smd connectors with extra soldering)

    int order;

    if (mountType & 1)
        order = (mountType & 2) ? 2 : 1;
    else
        order = (mountType & 2) ? 3 : 4;

    return order;
}

static int mountType( SExpr *modx )
{
    int mountCode = 0;  // bit 0: smd seen, bit 1: tht seen

    SExpr *pads = modx->filterByFirst("pad");
    for (int i = 0; i < pads->count(); i++) {
        SExpr *padx = pads->at(i);
        QString mountType = padx->at(2)->text();
        if (mountType == "smd")
            mountCode |= 1;
        else if (mountType == "thru_hole")
            mountCode |= 2;
    }
    pads->free();

    return mountCode;
}

BanzWindow::BanzWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::BanzWindow)
{
    ui->setupUi(this);
}

BanzWindow::~BanzWindow()
{
    delete ui;
}

void BanzWindow::processModules(SExpr *stree)
{
    int i;

    SExpr *mods = stree->filterByFirst("module");
    // mods->print();
    for (i = 0; i < mods->count(); i++) {
        SExpr* modx = mods->at(i);
        SExpr* refx = modx->findByFirst2("fp_text","reference");
        SExpr* atx = modx->findByFirst("at");
        SExpr *valx = modx->findByFirst2("fp_text","value");

        if (refx) {
            QString reftitle = refx->at(2)->text();
            QString xs = atx->at(1)->text();
            QString ys = atx->at(2)->text();
            QString rs = "0";
            if (atx->count() >= 4)
              rs = atx->at(3)->text();

            QString vals = valx->at(2)->text();
            QString packs = modx->at(1)->text();

 /*           cout << reftitle << " ";
            cout << xs << " " << ys << "  ";
            cout << rs << " " << vals << " " << forms << endl;
            cout.flush();
*/
            BAModule bm;
            bm.x = xs.toFloat();
            bm.y = ys.toFloat();
            bm.rot = rs.toFloat();
            bm.setRef( reftitle );
            bm.setValue( vals );
            bm.setPackage( packs );
            bm.mountType = mountType(modx);

            SExpr *fablx = modx->filterByFirst("fp_line");
            getLineList( fablx, bm.fablines, "F.Fab" );
            fablx->free();

            modlist.append(bm);
        }
    }
    mods->free();
}



void BanzWindow::processEdges(SExpr *stree)
{
    SExpr *lines = stree->filterByFirst("gr_line");
    getLineList( lines, edges, "Edge.Cuts" );
    lines->free();
}

static bool modRefLessThan( const BAModule &m1, const BAModule &m2 )
{
    QCollator coll;
    coll.setNumericMode(true);
    return coll.compare(m1.ref,m2.ref) < 0;
}

static bool modGeoLessThan( const BAModule &m1, const BAModule &m2 )
{
    if (m1.y < m2.y)
        return true;
    if (m1.y > m2.y)
        return false;

    if (m1.x < m2.x)
        return true;
    if (m1.x > m2.x)
        return false;

    QCollator coll;
    coll.setNumericMode(true);
    return coll.compare(m1.ref,m2.ref) < 0;
}


static bool modTVLessThan( const BAModule &m1, const BAModule &m2 )
{
    // 1.) order non-values (empty or in parentheses) to end

    if (m1.valueGood != m2.valueGood)
        return m1.valueGood;

    // 1a) order SMD first, no mount last

    int mo1 = mountOrder(m1.mountType);
    int mo2 = mountOrder(m2.mountType);
    if (mo1<mo2)
        return true;
    if (mo1>mo2)
        return false;

    // 2.) type of component (letters of reference)

    int cr = QString::compare(m1.refType, m2.refType);
    if (cr)
        return cr < 0;

    // 2a.) package

    QCollator coll;
    coll.setNumericMode(true);

    cr = coll.compare(m1.package, m2.package);
    if (cr)
        return cr < 0;

    // 3.) numeric value (only if good for both)

    if (m1.valueGood) {
        if (m1.valueNumeric < m2.valueNumeric)
            return true;
        if (m1.valueNumeric > m2.valueNumeric)
            return false;
    }

    // 4.) value as text (natural)



    cr = coll.compare(m1.val,m2.val);
    if (cr)
        return cr < 0;

    // 5.) reference (natural)

    cr = coll.compare(m1.ref,m2.ref);
    if (cr)
        return cr < 0;

    return false;
}

void BanzWindow::sortModules( int sortType )
{
    switch (sortType) {
    default:
    case 1: std::sort(modlist.begin(), modlist.end(),modRefLessThan);  break;
    case 2: std::sort(modlist.begin(), modlist.end(),modGeoLessThan);  break;
    case 3: std::sort(modlist.begin(), modlist.end(),modTVLessThan);  break;
    }
}

void BanzWindow::on_actionOpen_triggered()
{
    BAView *bav = ui->baView;
    QTextStream cout(stdout, QIODevice::WriteOnly);

    QString startDir;
    if (!filePath.isEmpty()) {
        QFileInfo fi(filePath);
        startDir = fi.absolutePath();
    }
    else {
        const char *home = getenv("HOME");
        if (home)
            startDir = home;
    }

    filePath = QFileDialog::getOpenFileName( this, tr("Open PCB Layout"), startDir, tr("Layout Files (*.kicad_pcb)"));
    QFile f(filePath);
    f.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream s(&f);

    modlist.clear();
    edges.clear();

    SExprLoader loader(&s);
    SExpr *stree = loader.load();
    this->processModules(stree);
    this->sortModules(1);
    this->processEdges(stree);

    // setup (data) model and connect to table view

    model.list = &modlist;
    QTableView* tv = ui->tableView;
    tv->setModel(&model);

    // setup selection model and connect to table view

    QItemSelectionModel* tsm = tv->selectionModel();
    if (tv->selectionModel() == NULL)
        tsm = new QItemSelectionModel( &model );
    connect( tsm, &QItemSelectionModel::selectionChanged,
                 this, &BanzWindow::on_table_selection );
    tv->setSelectionModel(tsm);
    tv->setSelectionBehavior(QAbstractItemView::SelectRows);
    tv->resizeColumnsToContents();

    // let pcb view know of the loaded list and redraw it
    bav->list = &modlist;
    bav->edges = &edges;
    bav->adaptToData();
    bav->repaint();

    stree->free();

    QFileInfo fi(filePath);
    this->setWindowTitle(fi.fileName());
}

void BanzWindow::on_table_selection(const QItemSelection &selected,const QItemSelection &deselected)
{
    int row;
    foreach( QModelIndex selix, selected.indexes() ) {
        row = selix.row();
        modlist[row].selected = true;
    }
    foreach( QModelIndex desix, deselected.indexes() ) {
        row = desix.row();
        modlist[row].selected = false;
    }
    ui->baView->repaint();
}

void BanzWindow::on_actionQuit_triggered()
{
    qApp->closeAllWindows();
}

void BanzWindow::writeCSV( QTextStream *s )
{
    s->setRealNumberNotation(QTextStream::FixedNotation);
    s->setRealNumberPrecision(4);

    // could've gotten those from the header...
    *s << "REF;X;Y;ROT;VALUE;PACK;MNT" << endl;

    int numRows = model.rowCount();
    int numCols = model.columnCount();

    for (int r = 0; r < numRows; r++) {
        for (int c = 0; c < numCols; c++ ) {
            QModelIndex mi = model.index(r,c,QModelIndex());
            QVariant cellData = model.data(mi);
            if (c != 0)
                *s << ";";
            if (cellData.type() == QVariant::Type::Double)
                *s << cellData.toDouble();
            else
                *s << cellData.toString();
        }
        *s << endl;
    }

    PointRec tl, br;
    bool haveBounds = getImageBounds( edges, tl, br );
    if (haveBounds) {
        *s << "#edges;" << tl.x << ";" << tl.y << ";" << br.x << ";" << br.y << endl;
    }

}

void BanzWindow::on_actionExport_triggered()
{
    if (filePath.isEmpty())
        return;

    QFileInfo fi(filePath);

    QString saveSuggested = fi.absolutePath();

    saveSuggested+="/";
    saveSuggested+=fi.baseName();
    saveSuggested+=".pnp.csv";

    QString saveName = QFileDialog::getSaveFileName(this, tr("Export Pick&Place CSV"), saveSuggested );
    if (!saveName.isEmpty()) {
        QFile saveFile(saveName);
        if (saveFile.open((QIODevice::WriteOnly))) {
            QTextStream saveStream(&saveFile);
            this->writeCSV( &saveStream );
        }
        else
            QMessageBox::information(this, tr("Unable to open for writing"),
                                     saveFile.errorString());
    }
}

void BanzWindow::refreshData(void)
{
    QTableView* tv = ui->tableView;
    tv->clearSelection();
    tv->setModel(&model);

    for (int i = 0; i < modlist.count(); i++)
        modlist[i].selected = false;
    ui->baView->repaint();
}
void BanzWindow::on_actionSortByRef_triggered()
{
    this->sortModules(1);
    this->refreshData();
}

void BanzWindow::on_actionSortByGeo_triggered()
{
    this->sortModules(2);
    this->refreshData();
}

void BanzWindow::on_actionSortByPlace_triggered()
{
    this->sortModules(3);
    this->refreshData();
}

void BanzWindow::zoom( double scale )
{
    BAView *bav = ui->baView;
    bav->scale = scale;
    bav->adaptToData();
    bav->repaint();
}
void BanzWindow::on_action50_triggered()
{
    this->zoom(2);
}

void BanzWindow::on_action75_triggered()
{
    this->zoom(3);
}

void BanzWindow::on_action100_triggered()
{
    this->zoom(4);
}

void BanzWindow::on_action150_triggered()
{
    this->zoom(6);
}

void BanzWindow::on_action200_triggered()
{
    this->zoom(8);
}

void BanzWindow::on_actionAbout_triggered()
{
    QMessageBox::about( this, "BAnzeKi 0.1",
                        "Bestueckungs-Anzeige fuer KiCAD\n"
                        "(Placement indicator for KiCAD)\n\n"
                        "Written and (C) 2019 by Richard Theil\n"
                        "This is free software under the GPL V2\n\n"
                        "Use 'footswitch -k down' (by rgerganov)\n"
                        "for stepping with a 'Microdia' foot switch."
                        );
}
