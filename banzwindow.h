/*
banzwindow.h
Copyright (C) 2019 Richard Theil

This program is free software; you can redistribute it and/or
modify it under the terms of version 2 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BANZWINDOW_H
#define BANZWINDOW_H

#include <QMainWindow>
#include <QItemSelection>
#include <QTextStream>

#include "baview.h"
#include "sexpr.h"

namespace Ui {
class BanzWindow;
}

class BanzWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit BanzWindow(QWidget *parent = 0);
    ~BanzWindow();

    QList<BAModule> modlist;
    QList<LineRec> edges;

    BAModuleListModel model;
    QString filePath;

     void processModules(SExpr *stree);
     void processEdges(SExpr *stree);

     void sortModules(int sortType);

     void writeCSV( QTextStream *s );

     void on_table_selection(const QItemSelection &,const QItemSelection &);

private slots:
    void on_actionOpen_triggered();
    void on_actionQuit_triggered();

    void on_actionExport_triggered();

    void refreshData(void);
    void on_actionSortByRef_triggered();
    void on_actionSortByGeo_triggered();
    void on_actionSortByPlace_triggered();

    void zoom( double scale );
    void on_action50_triggered();
    void on_action75_triggered();
    void on_action100_triggered();
    void on_action150_triggered();
    void on_action200_triggered();

    void on_actionAbout_triggered();

private:
    Ui::BanzWindow *ui;
};

#endif // BANZWINDOW_H
