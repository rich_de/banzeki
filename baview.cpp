/*
baview.cpp
Copyright (C) 2019 Richard Theil

This program is free software; you can redistribute it and/or
modify it under the terms of version 2 of the GNU General Public License
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>.
*/

#include "baview.h"

#include <QPainter>
#include <QPen>

#include <math.h>

BAModule::BAModule()
{
    x = 0;
    y = 0;
    rot = 0;
    selected = false;
    valueGood = true;
    valueNumeric = 0;
}

void BAModule::setRef( QString iref )
{
    ref = iref;

    int n = 0, ilen = iref.length();
    while (n < ilen && iref[n].isLetter())
        n++;

    if (n)
        refType = iref.left(n);
}

void BAModule::setValue( QString ival )
{
    bool couldBeShort = true;   // 4k7 notation

    valueNumeric = 0;
    valueGood = true;

    val = ival;
    int n = 0, ilen = ival.length();

    if (!ilen || ival[0] =='(') {
        valueGood = false;
        return;
    }

    // scan over characters that might be a numeric value

    QChar c;
    while (n < ilen) {
        c = ival[n];
        if (c.isDigit()) {
            n++;
        } else if (c == '-'||c == '+'||c == '.'||c=='E'||c=='e') {
            n++;
            couldBeShort = false;
        } else
            break;
    }
    if (n)
        valueNumeric = ival.left(n).toFloat();

    // skip possible whitespace before exponent prefix
    while (n < ilen && ival[n].isSpace()) {
        n++;
        couldBeShort = false;
    }

    if (n < ilen) {
        double scaler;
        c = ival[n++];
        ushort unic = c.unicode();
        switch (unic) {
        case 'p': scaler = 1.0E-12; break;
        case 'n': scaler = 1.0E-9; break;
        case 0x00B5: // U+00B5: mu-sign in unicode
        case 'u': scaler = 1.0E-6; break;
        case 'm': scaler = 1.0E-3; break;
        case 'k': scaler = 1.0E3; break;
        case 'M': scaler = 1.0E6; break;
        case 'G': scaler = 1.0E9; break;
        default: scaler = 1;
        }
        valueNumeric *= scaler;
        if (couldBeShort) {
            int fs = n;
            while (n < ilen && ival[n].isDigit())
                n++;
            if (fs != n) {
                QString frac = ival.mid(fs,n-fs);
                double ff = frac.toFloat();
                scaler *= pow(10.0,-(n-fs));
                valueNumeric += (ff*scaler);
            }
        }
    }
}

void BAModule::setPackage( QString ipack )
{
    QString last = ipack.section(':',-1);
    if (last.length())
        package = last;
    else
        package = ipack;
}

BAModuleListModel::BAModuleListModel(QObject *parent) : QAbstractTableModel(parent)
{
    list = 0;
}

int BAModuleListModel::rowCount(const QModelIndex &) const
{
    if (!list)
        return 0;

    int n = list->count();
    return n;
}
int BAModuleListModel::columnCount(const QModelIndex &) const
{
    return 7;
}

static const char * getMTName(int mountType)
{
    switch (mountType) {
    default:
    case 0: return "-";
    case 1: return "SMD";
    case 2: return "THT";
    case 3: return "S+T";
    }
}
QVariant BAModuleListModel::data(const QModelIndex &index, int role) const
{
    if (!list || role != Qt::DisplayRole)
        return QVariant();

    int row = index.row();
    switch (index.column())
    {
        case 0: return list->at(row).ref;
        case 1: return list->at(row).x;
        case 2: return list->at(row).y;
        case 3: return list->at(row).rot;
        case 4: return list->at(row).val;
        case 5: return list->at(row).package;
        case 6: return QString(getMTName(list->at(row).mountType));
    default:;
    }
    return QVariant();
}

QVariant BAModuleListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole){
        if (orientation == Qt::Horizontal) {
            switch (section) {
            case 0: return tr("REF");
            case 1: return tr("X");
            case 2: return tr("Y");
            case 3: return tr("ROT");
            case 4: return tr("VALUE");
            case 5: return tr("PACK");
            case 6: return tr("MNT");
            default:;
            }
        }
    }
    return QVariant();
}

BAView::BAView(QWidget *parent) : QWidget(parent)
{
    list = 0;
    edges = NULL;
    scale = 4.0;
}

void BAView::paintLine(QPainter &painter, LineRec &lrec)
{
    painter.drawLine( lrec.from.x * scale, lrec.from.y * scale,
                      lrec.to.x * scale, lrec.to.y * scale );

}

void BAView::paintLineList( QPainter &painter, QList<LineRec> &llist )
{
    foreach( LineRec lrec, llist ) {
        paintLine( painter, lrec );
    }
}

void BAView::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.fillRect( rect(), Qt::white );
    painter.setPen( Qt::black );

    if (edges)
        paintLineList( painter, *edges );

    if (list) {
        foreach( BAModule bm, *list ) {
            double x = bm.x * scale;
            double y = bm.y * scale;
            painter.save();
            painter.translate(x, y);
            painter.rotate(-bm.rot);
            if (bm.selected)
                painter.setPen( Qt::red );
            else
                painter.setPen( Qt::black );

            painter.drawText( 0, 0, bm.ref );
            if (bm.selected) {
                painter.setPen( QPen(Qt::red,3) );
                painter.drawLine( -60, 0, -30, 0 );
                painter.drawLine( 30, 0, 60, 0 );
                painter.drawLine( 0, -100, 0, -15 );
                painter.drawLine( 0, 15, 0, 40 );
                painter.setPen( QPen(Qt::red,2) );
            }
            else
                painter.setPen( Qt::lightGray );
            paintLineList( painter, bm.fablines );
            painter.restore();
        }
    } else
         painter.drawText( 20, 20, "Please open a PCB layout" );
}

void BAView::adaptToData(void)
{
    double x, y;
    double maxx = 0, maxy = 0;
    if (list) {
        foreach( BAModule bm, *list ) {
            x = bm.x * scale;
            y = bm.y * scale;
            if (x > maxx)
                maxx = x;
            if (y > maxy)
                maxy = y;
        }
        maxx += 200;
        maxy += 200;
        this->resize( maxx, maxy );
    }
}
